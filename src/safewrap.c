#include "safewrap.h"
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#ifndef __WIN32__

void _ignore_handler(const char *restrict msg, ErrorStruct *restrict ptr, int error)
{
    (void)error;
    printf("[Error] %s", msg);
    if (ptr)
    {
        printf(", %s in function %s:%zu\n", ptr->file, ptr->function_name, ptr->line);
    }
}

void _abort_handler(const char *restrict msg, ErrorStruct *restrict ptr, int error)
{
    printf("[Error] %s", msg);
    if (ptr)
    {
        printf(", %s in function %s:%zu\n", ptr->file, ptr->function_name, ptr->line);
    }
    exit(error);
}

constraint_handler_t _handler = NULL;

constraint_handler_t set_constraint_handler_s(constraint_handler_t handler)
{
    return _handler = handler;
}

void call_handler(const char *restrict msg, ErrorStruct *restrict ptr, int error)
{
    if (_handler)
    {
        _handler(msg, ptr, error);
    }
    _ignore_handler(msg, ptr, error);
}

char *_gets_s(char *str, size_t n, __ERROR_PARAMS__)
{
    MAKE_ERROR(e);
    if (n == 0)
    {
        call_handler("gets_s n cannot be 0", &e, -1);
    }
    if (str == NULL)
    {
        call_handler("gets_s str* canot be nullptr", &e, -1);
    }
    for (size_t i = 0; i < n; i++)
    {
        char c = getc(stdin);
        if (i == n - 1)
        {
            call_handler("gets_s no endline or eof found or name is too long", &e, -1);
        }
        if (c == '\n')
        {
            str[i] = '\0';
            break;
        }
        else
        {
            str[i] = c;
        }
    }
    return str;
}

size_t _strnlen_s(const char *str, size_t max_size, __ERROR_PARAMS__)
{
    MAKE_ERROR(e);
    if (!str)
    {
        call_handler("strlen_s str cannot be nullptr", &e, -1);
    }
    for (size_t i = 0; i < max_size; i++)
    {
        if (str[i] == '\0')
        {
            return i;
        }
    }
    return max_size;
}

int _strcat_s(char *restrict dest, size_t destsz, const char *restrict src, __ERROR_PARAMS__)
{
    MAKE_ERROR(e);
    if (destsz == 0)
    {
        call_handler("strcat_s destsz cannot be 0", &e, -1);
        return -1;
    }
    if (dest == NULL)
    {
        call_handler("strcat_s dest* cannot be nullptr", &e, -1);
        return -1;
    }
    size_t len = strnlen_s(dest, destsz);
    if (len == destsz)
    {
        call_handler("strcat_s dest* has no null terminator", &e, -1);
        return -1;
    }
    size_t available = destsz - len;
    size_t len2 = strnlen_s(src, available);
    if (len2 == available)
    {
        call_handler("strcat_s src* does not fit into the buffer or has no null terminator", &e, -1);
        return -1;
    }
    memcpy(dest + len, src, len2 + 1);
    return 0;
}

int _strcpy_s(char *restrict dest, size_t destsz, const char *restrict src, __ERROR_PARAMS__)
{
    MAKE_ERROR(e);
    if (destsz == 0)
    {
        call_handler("strcpy_s destsz cannot be 0", &e, -1);
        return -1;
    }
    if (dest == NULL)
    {
        call_handler("strcpy_s dest* cannot be nullptr", &e, -1);
        return -1;
    }
    size_t len = strnlen_s(src, destsz);
    if (len == destsz)
    {
        call_handler("strcpy_s src* does not fit into the buffer or has no null terminator", &e, -1);
        return -1;
    }
    memcpy(dest, src, len + 1);
    return 0;
}

int _strncpy_s(char *restrict dest, size_t destsz, const char *restrict src, uint64_t count, __ERROR_PARAMS__)
{
    MAKE_ERROR(e);
    if (destsz == 0)
    {
        call_handler("strncpy_s destsz cannot be 0", &e, -1);
        return -1;
    }
    if (dest == NULL)
    {
        call_handler("strncpy_s dest* cannot be nullptr", &e, -1);
        return -1;
    }
    size_t len = strnlen_s(src, count);
    if (len >= destsz)
    {
        call_handler("strncpy_s src* does not fit into the buffer", &e, -1);
        return -1;
    }
    memcpy(dest, src, len + 1);
    dest[len] = 0;
    return 0;
}

int _fopen_s(FILE *restrict *restrict streamptr, const char *restrict filename, const char *restrict mode,
             __ERROR_PARAMS__)
{
    MAKE_ERROR(e);
    if (streamptr == NULL)
    {
        call_handler("fopen_s streamptr cannot be nullptr", &e, -1);
        return -1;
    }
    if (filename == NULL)
    {
        call_handler("fopen_s filename cannot be nullptr", &e, -1);
        return -1;
    }
    if (mode == NULL)
    {
        call_handler("fopen_s mode cannot be nullptr", &e, -1);
        return -1;
    }
    *streamptr = fopen(filename, mode);
    if (*streamptr == NULL)
    {
        call_handler("fopen_s could not open file", &e, -1);
        return -1;
    }

    return 0;
}

// int _sscanf_s(const char *restrict buffer, const char *restrict format, ...)
// {
//     va_list args;
//     va_start(args, format);
//
//     return 0;
// }
#endif
