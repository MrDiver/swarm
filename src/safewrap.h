#pragma once
#include <stdint.h>
#include <stdio.h>

#ifndef __WIN32__

#define _countof(array) (sizeof(array) / sizeof(array[0]))

typedef struct ErrorStruct
{
    const size_t line;
    const char *file;
    const char *function_name;
} ErrorStruct;

// Definitions for constraint handlers which are called when one of the functions runs into undefined behavior
typedef void (*constraint_handler_t)(const char *restrict msg, ErrorStruct *restrict ptr, int error);
constraint_handler_t set_constraint_handler_s(constraint_handler_t handler);
void _abort_handler(const char *restrict msg, ErrorStruct *restrict ptr, int error);
void _ignore_handler(const char *restrict msg, ErrorStruct *restrict ptr, int error);

// Helper macros for defining functions which get error data
#define __ERROR_VALS__ __LINE__, __FILE__, __func__
#define __ERROR_PARAMS__ const size_t line, const char *file, const char *func
#define __ERROR_FORWARD__ line, file, func
#define MAKE_ERROR(e) ErrorStruct e = {line, file, func}

#define gets_s(...) _gets_s(__VA_ARGS__, __ERROR_VALS__)
char *_gets_s(char *str, size_t n, __ERROR_PARAMS__);

#define strnlen_s(...) _strnlen_s(__VA_ARGS__, __ERROR_VALS__)
size_t _strnlen_s(const char *str, size_t max_size, __ERROR_PARAMS__);

#define strcat_s(...) _strcat_s(__VA_ARGS__, __ERROR_VALS__);
int _strcat_s(char *restrict dest, size_t destsz, const char *restrict src, __ERROR_PARAMS__);
// Maybe strncat_s

#define strcpy_s(...) _strcpy_s(__VA_ARGS__, __ERROR_VALS__)
int _strcpy_s(char *restrict dest, size_t destsz, const char *restrict src, __ERROR_PARAMS__);

#define strncpy_s(...) _strncpy_s(__VA_ARGS__, __ERROR_VALS__)
int _strncpy_s(char *restrict dest, size_t destsz, const char *restrict src, uint64_t count, __ERROR_PARAMS__);

#define fopen_s(...) _fopen_s(__VA_ARGS__, __ERROR_VALS__)
int _fopen_s(FILE *restrict *restrict streamptr, const char *restrict filename, const char *restrict mode,
             __ERROR_PARAMS__);

// #define sscanf_s(...) _sscanf_s(__VA_ARGS__)
// int _sscanf_s(const char *restrict buffer, const char *restrict format, ...);

#endif
